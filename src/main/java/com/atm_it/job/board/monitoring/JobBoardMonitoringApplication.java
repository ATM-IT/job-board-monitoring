package com.atm_it.job.board.monitoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
@EnableDiscoveryClient
public class JobBoardMonitoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobBoardMonitoringApplication.class, args);
    }

}
